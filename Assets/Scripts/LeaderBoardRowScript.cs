﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeaderBoardRowScript : MonoBehaviour
{
    public Text NumberText;
    public Text TitleText;
    public Text ScoreText;

    string Number = "";
    string Title = "";
    int Score = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetData(string _number, string _title, int _score) {
        Number = _number;
        Title = _title;
        Score = _score;

        NumberText.text = Number;
        TitleText.text = Title;
        ScoreText.text = Score.ToString();
    }
}
