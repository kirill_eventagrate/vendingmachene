﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityOSC;
using UnityEngine.SceneManagement;
public class OSCManager : MonoBehaviour
{

    public static OSCManager instance = null;

    // osc reciver
    public int port = 7000;

    // osc sender
    private OSCClient server;
    private string ip;

    // Use this for initialization
    void Start()
    {
        InitOSC();

    }

    private void Awake()
    {
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

    public void InitOSC()
    {
        ip = "255.255.255.255";
        server = new OSCClient(System.Net.IPAddress.Parse(ip), port);
        Debug.Log("Connected");
    }


    public void SendOSCValue(string name , string score)
    {
        if (server != null)
        {
            Loom.QueueOnMainThread(() => {

                var message = new OSCMessage("/leaderboard", name + "," + score);
                server.Send(message);
            });

        }
    }

}



