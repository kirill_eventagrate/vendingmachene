﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour {

    public int points = 0;

    TMPro.TextMeshPro[] allPointText;

    Material mat;

    bool enableTouch = true;
    public float speed = 10;

    bool isClicked = false;

    private void Start()
    {
        mat = GetComponent<MeshRenderer>().material;
    }
    public void UpdateText(bool isRed)
    {
        allPointText = transform.GetComponentsInChildren<TMPro.TextMeshPro>();
        if (isRed)
        {
            points = Random.Range(-8, -1);
        }
        else
        {
            List<int> pointsList = new List<int>() { 6, 8, 16, 7 };
            int randomIndex = Random.Range(0, 4);
            points = pointsList[randomIndex];
        }

        string toSet = points.ToString();
        if (isRed)
        {
            toSet = toSet.Trim('-');
        }
            

        foreach (TMPro.TextMeshPro item in allPointText)
        {
            item.text = toSet;
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
		if (transform.position.y < -50)
        {
            Destroy(transform.gameObject);
        }

        //transform.Rotate(new Vector3(0.05f, 0.05f, 0.05f));
        transform.Rotate(new Vector3(1, 1, 1) * Time.deltaTime * speed);
    }

    private void OnMouseDown()
    {
        if (GameManager.instance.currentState == GameManager.GameState.inGame && enableTouch && !isClicked)
        //if (!isClicked)
        {
            //Debug.Log("Clicked");
            isClicked = true;
            enableTouch = false;
            GameManager.instance.AddScore(points);
            //transform.GetComponentInChildren<ParticleSystem>().Stop();
            //Destroy(this.gameObject);
            //StartCoroutine(Disintegrate());
            ScaleDownAnimation();
        }
    }

    void ScaleDownAnimation()
    {
        iTween.ScaleTo(this.gameObject, iTween.Hash(
         "scale", Vector3.zero,
         "easetype", iTween.EaseType.easeOutBack,
         "time", 0.35f,
         "oncompletetarget", this.gameObject,
         "oncomplete", "ScaleDownComplete"));
    }

    void ScaleDownComplete()
    {
        Destroy(this.gameObject);
    }

    /*IEnumerator Disintegrate()
    {
        float dissolveVal = mat.GetFloat("_Fill");
        while(dissolveVal > 0)
        {
            yield return new WaitForSeconds(0.02f);
            dissolveVal -= 0.05f;
            mat.SetFloat("_Fill", dissolveVal);
        }

        Destroy(this.gameObject);
    }*/

    IEnumerator Disintegrate()
    {
        float dissolveVal = mat.GetFloat("_Cutout");
        while (dissolveVal < 0.5f)
        {
            yield return new WaitForSeconds(0.02f);
            dissolveVal += 0.05f;
            mat.SetFloat("_Cutout", dissolveVal);
        }

        Destroy(this.gameObject);
    }
}
