﻿using HeathenEngineering.UIX;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public static GameManager instance;

    public int score = 0;

    float gameTime = 30.0f;

    public GameObject gameOverCanvas;
    public Text scoreTextGameOver;

    public CubeSpawner cubeSpawner;

    public Text countdownTimerText;

    public GameObject inGamePanel;
    public Text scoreTextInGame;

    public Text timePanelInGame;

    public GameObject mainScreen, rulesScreen, countDownScreen;
    public InputField[] userInputFields;
    

    public Transform cubesParent;

    public RectTransform inputPanel;
    public GameObject keyboard;

    public GameObject LeaderBoardPanel;
    public GameObject LeadersContainer;
    public GameObject RowOfLeaderboard;

    public GameObject TermsAndPolicies;
    public Toggle AgreeCheckBox;

    string pathCSV;
    User user = new User();
    List<User> Users = new List<User>();
    bool dataIsSend = false;
    int time = 3;
    public string EmailAdresses = "";
    public int Hour = 0, Min = 0;
    public int[] KindOfPrisesIntervals = new int[4];

    public enum GameState
    {
        mainScreen,
        rulesScreen,
        countDown,
        inGame,
        waitForAllCubes,
        leaderboard,
        endScreen
    }

    public GameState currentState;
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }
    // Use this for initialization
    void Start ()
    {
        currentState = GameState.mainScreen;
        //ReadTimeFile();
        PrepareUserDataFile();
        InvokeRepeating("CheckTimeForSendUsersData", 0f, 60f);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (currentState == GameState.inGame)
        {
            gameTime -= Time.deltaTime;
            timePanelInGame.text = ((int)gameTime).ToString();
            if (gameTime < 0)
            {
                currentState = GameState.waitForAllCubes;
                timePanelInGame.text = "0";
                scoreTextGameOver.text = score.ToString();
                cubeSpawner.StopSpawning();
            }
        }

        else if (currentState == GameState.waitForAllCubes)
        {
            if (cubesParent.childCount == 0)
            {
                currentState = GameState.endScreen;
                gameOverCanvas.SetActive(true);
                inGamePanel.SetActive(false);
                //ShowLeaderBoard();
                
                
                user.SetScore(score);
                //AddUserToUserList(user);
                ChoosePrizes();
                //Invoke("ShowLeaderBoard", 5);
                // Write user data to file
                SaveUserDataToFile();
                Invoke("GoToMain", 5);
                //OSCManager.instance.SendOSCValue(userInputFields[0].text , scoreTextGameOver.text);
            }
        }
    }

    public void ShowLeaderBoard()
    {
        currentState = GameState.leaderboard;
        for (int i = 0; i < LeadersContainer.transform.childCount; i++)
        {
            Destroy(LeadersContainer.transform.GetChild(i).gameObject);
        }
        int count = 1;
        
        foreach (User us in Users)
        {
            if (count <= 6)
            {
                GameObject obj = Instantiate(RowOfLeaderboard, LeadersContainer.transform);
                //obj.transform.SetParent(LeadersContainer.transform);
                obj.GetComponent<RectTransform>().localScale = Vector3.one;
                obj.GetComponent<LeaderBoardRowScript>().SetData(count.ToString(), us.GetName() + " " + us.GetSurnamee(), us.GetScore());
                count++;
            }
            else { break; }
        }
        LeaderBoardPanel.SetActive(true);
        Invoke("GoToMain", 5);
    }

    void AddUserToUserList(User _user) {
        Users.Add(_user);
        Users = Users.OrderByDescending(us => us.GetScore()).ToList();
    }

    void CheckTimeForSendUsersData() {
        if (currentState == GameState.mainScreen && !dataIsSend) {
            Debug.Log(DateTime.Now.Hour + ":" + DateTime.Now.Minute);
            if (DateTime.Now.Hour == Hour && (DateTime.Now.Minute >= (Min - 1) && DateTime.Now.Minute <= (Min + 1))) {
                ThreadStart threadStart = new ThreadStart(SendMail);
                Thread thread = new Thread(threadStart);
                thread.IsBackground = true;
                thread.Start();
                dataIsSend = true;
                CancelInvoke("CheckTimeForSendUsersData");
                Debug.Log("Send Data");
            }
        }
    }

    void GoToMain()
    {
        SceneManager.LoadScene(0);
        /*ClearInputFields();
        LeaderBoardPanel.SetActive(false);
        gameOverCanvas.SetActive(false);
        inGamePanel.SetActive(false);
        rulesScreen.SetActive(false);
        countDownScreen.SetActive(false);

        currentState = GameState.mainScreen;
        gameTime = 30.0f;
        PrepareUserDataFile();
        time = 3;
        score = 0;
        mainScreen.SetActive(true);*/
    }

    void ClearInputFields() {
        foreach (InputField inputField in userInputFields){
            inputField.text = "";
        }
    }

    
    public void SubmitPressed()
    {
        if (CheckUserDataInputFields() && AgreeCheckBox.isOn)
        {
            user = new User(userInputFields[0].text, userInputFields[1].text, userInputFields[2].text, userInputFields[3].text, userInputFields[4].text);
            keyboard.SetActive(false);
            currentState = GameState.rulesScreen;
            rulesScreen.SetActive(true);
            mainScreen.SetActive(false);
        }
    }

    public void RulesNextPressed()
    {
        rulesScreen.SetActive(false);
        countDownScreen.SetActive(true);
        currentState = GameState.countDown;
        countdownTimerText.text = time.ToString();
        InvokeRepeating("ShowCountdownTime", 1, 1);
    }

    void ShowCountdownTime()
    {
        time--;
        countdownTimerText.text = time.ToString();
        if (time == 0)
        {
            countDownScreen.SetActive(false);
            cubeSpawner.StartSpawning();
            CancelInvoke("ShowCountdownTime");
            inGamePanel.SetActive(true);
            currentState = GameState.inGame;
        }
    }

    public void AddScore(int val)
    {
        score += val;
        scoreTextInGame.text = score.ToString() ;
    }

    public void OnInputFieldClick(int _fieldIndex)
    {
        inputPanel.anchoredPosition = new Vector2(0, 100);
        keyboard.GetComponent<Keyboard>().linkedGameObject = userInputFields[_fieldIndex].gameObject;
        keyboard.GetComponent<Keyboard>().linkedBehaviour = userInputFields[_fieldIndex];
        keyboard.SetActive(true);
    }

    public void OnInputEnd()
    {
        //inputPanel.anchoredPosition = new Vector2(0, -110);
        //keyboard.SetActive(false);
    }

    

    void PrepareUserDataFile() {
        pathCSV = "Users.csv";  // 
        if (!File.Exists(pathCSV))
        {
            StreamWriter csv = new StreamWriter(pathCSV, true, Encoding.UTF8);
            csv.WriteLine("Date;Name;Surname;CompanyName;Phone;Email;Score");
            csv.Close();
        }
        string intervalsFile = "ScoreIntervals.txt";
        if (!File.Exists(intervalsFile)) {
            StreamWriter csv = new StreamWriter(intervalsFile, true, Encoding.UTF8);
            csv.WriteLine("150");
            csv.WriteLine("250");
            csv.WriteLine("350");
            csv.WriteLine("450");
            csv.Close();
        }

        string GameSettingsFile = "GameSettings.txt";
        if (!File.Exists(GameSettingsFile))
        {
            StreamWriter csv = new StreamWriter(GameSettingsFile, true, Encoding.UTF8);
            csv.WriteLine("30");
            csv.WriteLine("22:10");
            csv.WriteLine("k9131471912@gmail.com, kirill@eventagrate.com");
            csv.Close();
        }

        
        StreamReader theReader = new StreamReader(intervalsFile, Encoding.UTF8);
        string line = "";
        for (int i = 0; i < 4; i++)
        {
            line = theReader.ReadLine();

            if (line != null)
            {
                KindOfPrisesIntervals[i] = int.Parse(line);
            }
            else { break; }
        }


        theReader = new StreamReader(GameSettingsFile, Encoding.UTF8);
        line = "";
        // Take game time
        line = theReader.ReadLine();
        if (line != null){ gameTime = float.Parse(line); }
        // Take time for send users data
        line = theReader.ReadLine();
        if (line != null) {
            string[] splitStr = line.Split(':');
            Hour = int.Parse(splitStr[0]);
            Min = int.Parse(splitStr[1]);
        }
        // Take emails
        line = theReader.ReadLine();
        if (line != null) { EmailAdresses = line; }
    }

    void ChoosePrizes() {
        if (score > KindOfPrisesIntervals[0] && score < KindOfPrisesIntervals[1]){
            // Prize 1
        } else if (score > KindOfPrisesIntervals[1] && score < KindOfPrisesIntervals[2]) {
            // Prize 2
        } else if (score > KindOfPrisesIntervals[2] && score < KindOfPrisesIntervals[3]) {
            // Prize 3
        } else if (score > KindOfPrisesIntervals[3]) {
            // Prize 4
        }
    }

    void SaveUserDataToFile()
    {
        DateTime Today = DateTime.Today; 
        StreamWriter UserDataWriter = new StreamWriter(pathCSV, true, System.Text.Encoding.UTF8);
        string saveLine = Today.Month + "-" + Today.Day + "-" + Today.Year + ";" + user.GetStringLine();
        UserDataWriter.WriteLine(saveLine);
        UserDataWriter.Close();
    }

    bool CheckUserDataInputFields() {
        foreach (InputField input in userInputFields)
        {
            if (input.text.Trim() == "") {
                return false;
            }
        }
        return true;
    }

    public void ShowTermsPopup(bool _show) { TermsAndPolicies.SetActive(_show); }

    private void SendMail()
    {
        DateTime Today = DateTime.Today;
        //smtp сервер 
        string smtpHost = "smtp.sendgrid.net";
        //smtp порт 
        int smtpPort = 587;
        //логин 
        string login = "eventagrate";
        //пароль 
        string pass = "Eventagrate1";
        //создаем подключение
        SmtpClient client = new SmtpClient(smtpHost, smtpPort);

        client.Credentials = ((System.Net.ICredentialsByHost)(new NetworkCredential(login, pass)));

        //От кого письмо
        string from = "info@eventagrate.com";
        //Кому письмо
        string to = EmailAdresses;
        //Тема письма
        string subject = "User data " + Today.Month + "-" + Today.Day + "-" + Today.Year;
        //Текст письма
        string body = "";

        //Создаем сообщение
        MailMessage mess = new MailMessage(from, to, subject, body);
        Attachment attach = new Attachment (pathCSV);
		mess.Attachments.Add (attach);

        try
        {
            client.Send(mess);
        }
        catch (System.Exception ex)
        {
        }
    }
}

class User {

    string Name = "";
    string Surname = "";
    string CompanyName = "";
    string Email = "";
    string Phone = "";
    int Score = 0;

    public User() { }

    public User(string _name, string _surname, string _companyName, string _phone, string _email) {
        Name = _name;
        Surname = _surname;
        CompanyName = _companyName;
        Phone = _phone;
        Email = _email;
        Score = 0;
    }

    public void SetScore(int _score) { Score = _score; }

    public string GetName() { return Name; }
    public string GetSurnamee() { return Surname; }
    public string GetCompanyName() { return CompanyName; }
    public string GetEmail() { return Email; }
    public string GetPhone() { return Phone; }
    public int GetScore() { return Score; }
    public string GetStringLine() { return Name + ";" + Surname + ";" + CompanyName + ";" + Phone + ";" + Email + ";" + Score; }
}