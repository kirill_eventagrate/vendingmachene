﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeSpawner : MonoBehaviour {

    public GameObject cube;
    public GameObject redCube;
	// Use this for initialization
	void Start () {
        
	}

    public void StartSpawning()
    {
        InvokeRepeating("SpawnCube", 0, 0.3f);
    }

    void SpawnCube()
    {
        int xPos = 0;
        int yPos = 0;
        float force = 0;
       
        xPos = Random.Range(-4, 4);
        yPos = -9;
        force = Random.Range(600, 780);

        GameObject go;
        if(Random.Range(0.0f , 1.0f) > 0.75f)
        {
            go = Instantiate(redCube);
            go.transform.GetComponent<Cube>().UpdateText(true);
        }
        else
        {
            go = Instantiate(cube);
            go.transform.GetComponent<Cube>().UpdateText(false);
        }
        go.transform.GetComponent<Cube>().speed = Random.Range(80, 200);
        go.transform.position = new Vector3(xPos, yPos, 1);
        go.GetComponent<Rigidbody>().AddForce(new Vector3(0, force, 0), ForceMode.Impulse);
        go.transform.parent = transform;
        
    }

    bool isLaneOccupied(int xpos)
    {
        foreach(Transform item in transform)
        {
            if (item.position.x == xpos)
                return true;
        }

        return false;
    }


    public void StopSpawning()
    {
        CancelInvoke("SpawnCube");
    }
}
